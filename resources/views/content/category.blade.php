@extends('layouts.app')

@section('content')
    <h1>My blog: {{ $category->category }}</h1>
        @foreach ($category->messages as $message) 
            <h2>{{ $message->title }} <span>{{ $message->created_at }}</span></h2>
            <p>{{ $message->discription }}</p>
            <ul>
                @foreach ($message->tag as $tag)
                    <li>{{ $tag->tag }}</li>
                @endforeach
            </ul>
            <p><a href="/message/{{ $message->id }}">Comments</a></p>
        @auth
            <p><a href="/editmessage/{{ $message->id }}">Edit message</a></p>
        @endauth
        @endforeach
@endsection

