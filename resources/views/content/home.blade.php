@extends('layouts.app')

@section('content')
    <h1>Welcome to my Webblog</h1>
    @foreach($categories as $category)
        <a href="/category/{{ $category->id }}">{{ $category->category }}</a>
    @endforeach

@endsection