@extends('layouts.app')

@section('content')
    <h1>My blog: {{ $message->category->category }}</h1>
    <div>
        <h2>{{ $message->title }} <span>{{ $message->created_at }}</span></h2>
        <p>{{ $message->discription }}</p>
    </div>

    <div>
        <h2>Comments:</h2>
        @foreach ($message->comments as $comment)
            <div>
                <p>{{ $comment->comment }}</p>
                <p>{{ $comment->name }} @ {{ $comment->created_at }}</p>
            </div>
        @endforeach
        </div>
        <div>
            <h2>Write a comment!</h2>
            <form method="POST" action="/comment">
                @csrf
                <input type="hidden" name="message_id" value="{{ $message->id }}" />
                <p><label>comment: <textarea name='comment' type='text' required></textarea></label></p>
                <p><label>Name: <input name='name' type='text' required/><label></p>
                <p><input type="submit" name="submit" value="Submit!" /></p>
            </form>
            @if($errors->any())
            <div>
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
    </div>
@endsection