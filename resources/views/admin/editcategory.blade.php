@extends('layouts.app')

@section('content')
<h1>Edit category</h1>
    <form method="POST" action="/admin/category/{{ $category->id }}">
    @csrf
    {{ method_field('PATCH') }}
        <table>
            <tr>
                <td>New category name:</td>
                <td><input type="text" name="category" value="{{ $category->category }}" /></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" /></td>
            </tr>
        </table>
    </form>
    <form method="POST" action="/admin/category/{{ $category->id }}">
        @csrf
        {{ method_field('DELETE') }}
        <input type="Submit" value="Delete" />
    </form>
@endsection