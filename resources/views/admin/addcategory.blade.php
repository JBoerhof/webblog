@extends('layouts.app')

@section('content')
<h1>Add a new category to your blog</h1>
    <form method="POST" action="/admin/category">
    @csrf
        <p>
            <input name="category" type="text" />
            <input type="submit"/>
        </p>
    </form>
@endsection