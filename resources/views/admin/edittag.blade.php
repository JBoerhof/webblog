@extends('layouts.app')

@section('content')
<h1>Edit tag</h1>
    <form method="POST" action="/edittag/{{ $tag->id }}">
    @csrf
    {{ method_field('PATCH') }}
        <table>
            <tr>
                <td>New tag name:</td>
                <td><input type="text" name="tag" value="{{ $tag->tag }}" /></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" /></td>
            </tr>
        </table>
    </form>
@endsection