@extends('layouts.app')

@section('content')
<h1>Overview</h1>
    <table border="1">
    @foreach($categories as $category)
    <tr>
        <th>Category: {{ $category->category }}</th>
        <td><a href="/admin/category/{{ $category->id }}/edit">edit</a></td>
    </tr>
        @foreach($category->messages as $message)
        <tr>
            <td width="350">{{ $message->title }}</td>
            <td width="50"><a href="/admin/message/{{ $message->id }}/edit">edit</a></td>
        </tr>
        @endforeach
    @endforeach
    <tr>
        <th colspan="2"><a href="/admin/category/create">Add category</a>/<a href="/admin/message/create">Add Message</a></th>
    </tr>
    </table>

@endsection