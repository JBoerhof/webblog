@extends('layouts.app')

@section('content')
<h1>Add a new message to your blog</h1>
    <form method="POST" action="/admin/message">
        @csrf
        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" />
        <table border="1">
        <tr>
            <td>Title:</td>
            <td><input name='title' type='text' required/></td>
        </tr>
        <tr>
            <td>Category:</td> 
            <td><select name="category_id">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->category }}</option>
                @endforeach
                </select>
            </td>
        </tr>
        <tr>
            <td>Discription:</td>
            <td><textarea name='discription' type='text' required></textarea></td>
        <tr>
        <tr>
            <td colspan="2">Tags: </td>
        @foreach($tags as $tag)
        <tr>
            <td>{{ $tag->tag }}</td>
            <td><input name="tags[]" type="checkbox" value="{{ $tag->id }}"></option></td>
        </tr>
        @endforeach
        <tr>
            <td colspan="2"><input type="submit" name="submit" value="Submit!" /></td>
        </tr>
        </table>
    </form>
    @if($errors->any())
    <div>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
@endsection