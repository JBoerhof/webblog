@extends('layouts.app')

@section('content')
    <h1>Edit message:</h1>
    <form method="POST" action="/admin/message/{{ $message->id }}">
        @csrf
        {{ method_field('PATCH') }}
        <table border="1">
            <tr>
                <td>Title:</td>
                <td><input type="text" name="title" value="{{ $message->title }}" required/></td>
            </tr>
            <tr>
                <td>Discription:</td>
                <td><textarea name="discription" required>{{ $message->discription }}</textarea></td>
            </tr>
            <tr>
                <th colspan="2">Tags: </th>
            </tr>
            @foreach($tags as $tag)
                @if($message->tag->contains($tag->id) == true)
                    <tr>
                        <td>{{ $tag->tag }}</td> 
                        <td><input name="tags[]" type="checkbox" value="{{ $tag->id }}" checked /></td>
                    </tr>
                @else
                    <tr>
                        <td>{{ $tag->tag }}</td>
                        <td><input name="tags[]" type="checkbox" value="{{ $tag->id }}" /></td>
                    </tr>
                @endif
            @endforeach
        <tr>
            <td colspan="2">Date created: {{ $message->created_at }}</td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Edit"></td>
        </tr>
    </table>
    </form>
    <form method="POST" action="/admin/message/{{ $message->id }}">
        @csrf
        {{ method_field('DELETE') }}
        <input type="Submit" value="Delete" />
    </form>
    <h2>Comments:</h2>
        @foreach ($message->comments as $comment)
            <div>
                <p>{{ $comment->comment }}</p>
                <p>{{ $comment->name }} @ {{ $comment->created_at }}</p>
                <form method="POST" action="/admin/comment/{{ $comment->id }}">
                    @csrf
                    {{ method_field('DELETE') }}
                    <input type="submit" value="Delete this comment!"/>
                </form>
            </div>
        @endforeach
        </div>
@endsection

