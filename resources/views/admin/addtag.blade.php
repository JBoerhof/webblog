@extends('layouts.app')

@section('content')
<h1>Tags:</h1>
    <table border="1">
    @foreach($tags as $tag)
        <tr>
            <td>{{ $tag->tag }}</td>
            <td><a href="edittag/{{$tag->id}}">edit</a>/<a href="/deletetag/{{$tag->id}}">delete</a></td>
        </tr>
    @endforeach
    </table>
<h1>Add a new tag to your blog</h1>
    <form method="POST" action="addtag">
    @csrf
        <p>
            <input name="type" type="hidden" value="addtag" />
            <input name="addtag" type="text" />
            <input type="submit"/>
        </p>
    </form>
@endsection