@extends('layouts.app')

@section('content')

<form method="POST" action="/admin/user/{{ $user->id }}">
    @csrf
    {{ method_field('PATCH') }}
    <table>
        <tr>
            <td>Name:</td>
            <td><input type="text" name="name" value="{{ $user->name }}" /></td>
        </tr>
        <tr>
            <td>E-mail:</td>
            <td><input type="text" name="email" value="{{ $user->email }}"/></td>
        </tr>
        <tr>
            <td>Role</td>
            <td>
                <select name="role_id">
                
                @foreach($roles as $role)
                @if($user->role->id == $role->id) 
                    <option value="{{ $role->id }}" selected>{{ $role->name }}</option>
                @else
                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                @endif
                @endforeach
            </td>
        </tr>
        <tr>
            <td><input type="Submit" /></td>
            <td></td>
        </tr>
    </table>
</form>
@endsection
