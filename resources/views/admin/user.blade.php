@extends('layouts.app')

@section('content')
<h1>Users:</h1>
<table border="1">
    <tr>
        <td>Name</td>
        <td>email</td>
        <td>Role</td>
        <td>Created</td>
        <td></td>
    </tr>
    @foreach($users as $user)
    <tr>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->role->name }}</td>
        <td>{{ $user->created_at }}</td>
        <td><a href="/admin/user/{{ $user->id }}/edit">Edit</a></td>
    </tr>
    @endforeach
</table>
@endsection