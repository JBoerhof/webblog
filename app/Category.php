<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function messages(){
        return $this->hasMany('App\Message')->orderBy('created_at', 'DESC');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category'
    ];
}
