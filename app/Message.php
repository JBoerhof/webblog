<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function comments(){
        return $this->hasMany('App\Comment')->orderBy('created_at', 'DESC');
    }

    public function category(){
        return $this->belongsTo('\App\Category', 'category_id');
    }

    public function tag(){
        return $this->belongsToMany('App\Tag', 'pivot_message_tag', 'message_id', 'tag_id')->withPivot('id');
    }
    
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'category_id',  'title', 'discription'
    ];
}
