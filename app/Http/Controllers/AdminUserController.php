<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use Session;
use Redirect;

class AdminUserController extends Controller
{

    public function index()
    {
        $users = \App\User::All();
        
        return view('/admin/user', compact('users'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(User $user)
    {
        //
    }

    public function edit(User $user)
    {
        $roles = \App\Role::All();
        return view('/admin/edituser', compact('user', 'roles'));
    }

    public function update(Request $request, User $user)
    {
        request()->validate([
            'role_id' => 'required',
            'name' => 'required',
            'email' => 'required'
        ]);
        
        $user->role_id = request('role_id');
        $user->name = request('name');
        $user->email = request('email');

        $user->save();
                
        Session::flash('message', 'User edited!');
        
        return Redirect('/admin/user');
    }

    public function destroy(User $user)
    {
        //
    }
}
