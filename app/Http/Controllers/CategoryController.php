<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Session;
use Redirect;

class CategoryController extends Controller
{
  
    public function index()
    {
        $categories = \App\Category::orderBy('created_at', 'DESC')->get();

        return view('/content/home', compact('categories'));
    }

    public function show(Category $category)
    {
        return view('content/category', compact('category'));
    }

}
