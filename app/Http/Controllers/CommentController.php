<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Session;
use Redirect;

class CommentController extends Controller
{

    public function store(Request $request)
    {
        request()->validate([
            'message_id' => 'required',
            'name' => 'required',
            'comment' => 'required'
        ]);

        Comment::create(request(['message_id', 'name', 'comment']));

        Session::flash('message', "Comment added!");

        return Redirect::back();
    }
}
