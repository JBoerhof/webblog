<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Comment;
use App\Message;
use App\Tag;
use Session;
use Redirect;


class PostsController extends Controller
{

    public function index()
    {
        $categories = \App\Category::orderBy('created_at', 'DESC')->get();

        return view('/welcome', compact('categories'));
    }

    public function indexMessages($id)
    {
        $category = \App\Category::find($id);
        
       // dd($category);

        return view('content/index', compact('category'));
    }

    public function createCategory()
    {
        $categories = \App\Category::All();

        return view('content/addcategory', compact('categories'));
    }
    
    public function createMessage()
    {
        $categories = \App\Category::All();
        $tags = \App\Tag::All();;

        return view('content/addmessage', compact('categories', 'tags'));
    }

    public function createTag()
    {
        $tags = \App\Tag::All();;

        return view('content/addtag', compact('tags'));
    }

    public function store(Request $request)
    {
        if ($request->input('type') == 'message'){
            
            request()->validate([
                'user_id' => 'required',
                'category_id' => 'required',
                'title' => 'required',
                'discription' => 'required'
            ]);
            
            Message::create(request(['user_id', 'category_id', 'title', 'discription']));
            
            $message = Message::orderBy('created_at', 'desc')->first();
            
            $message->tag()->attach($request->input('tags'));

            Session::flash('message', "Message added!");
        
            return Redirect('/messages/'. $request->input('category_id'));

        } elseif ($request->input('type') == 'addcategory'){
        
            request()->validate([
                'addcategory' => 'required'
            ]);
           
            Category::create(['category' => request(['addcategory'])[$request->input('type')]]);

            Session::flash('message', "Category added!");
    
            return Redirect('/addcategory');
                
        } elseif ($request->input('type') == 'addtag'){
        
            request()->validate([
                'addtag' => 'required'
            ]);
            
            Tag::create(['tag' => request(['addtag'])[$request->input('type')]]);

            Session::flash('message', "Tag added!");
    
            return Redirect('/addtag');
                
        } else {
    
            request()->validate([
                'message_id' => 'required',
                'name' => 'required',
                'comment' => 'required'
            ]);

            Comment::create(request(['message_id', 'name', 'comment']));

            Session::flash('message', "Comment added!");

            return Redirect::back();
        }
    }

    public function show($id)
    {
        $message = \App\Message::find($id);
        return view('content/comment', compact('message'));
    }

    public function showcomment($id)
    {
        //
    }

    public function editMessage($id)
    {
        $message = \App\Message::find($id);
        $tags = \App\Tag::All();

      //  dd($message->tag);
        return view('content/editmessage', compact('message', 'tags'));
    }

    public function editCategory($id)
    {
        $category = \App\Category::find($id);

        return view('content/editcategory', compact('category'));
    }

    public function editTag($id)
    {
        $tag = \App\Tag::find($id);

        return view('content/edittag', compact('tag'));
    }

    public function updateMessage(Request $request, $id)
    {
        request()->validate([
            'title' => 'required',
            'discription' => 'required'
        ]);
        
        $message = Message::find($id);
        
        $message->title = request('title');
        $message->discription = request('discription');
        
        $message->tag()->sync($request->input('tags'));

        $message->save();
                
        Session::flash('message', 'Message edited!');
        
        return Redirect::back();

    }

    public function updateCategory(Request $request, $id)
    {
        request()->validate([
            'category' => 'required'
        ]);
        
        $category = Category::find($id);
        
        $category->category = request('category');

        $category->save();
                
        Session::flash('message', 'Category edited!');
        
        return Redirect('/addcategory');

    }

    public function updateTag(Request $request, $id)
    {
        request()->validate([
            'tag' => 'required'
        ]);
        
        $tag = Tag::find($id);
        
        $tag->tag = request('tag');

        $tag->save();
                
        Session::flash('message', 'Tag edited!');
        
        return Redirect('/addtag');

    }

    public function destroyMessage($id)
    {
        $message = Message::find($id);
        
        $categoryid = $message->category_id;

        $message->delete();

        Session::flash('message', 'Message deleted!');
        
        return Redirect('/messages/'.$categoryid);
    }

    public function destroyComment($id)
    {
        $comment = Comment::find($id);
        
        $messageid = $comment->message_id;

        $comment->delete();

        Session::flash('message', 'Comment deleted!');
        
        return Redirect('/editmessage/' . $messageid);
    }

    public function destroyTag($id)
    {
        $tag = Tag::find($id);
        try{$tag->delete();}
        catch (\Exception $e) {
            Session::flash('message', 'Not possible to delete the tag, probably still used for messages.');
            return Redirect::back();
          }
        Session::flash('message', 'Tag deleted!');
        return Redirect::back();
    }

    public function destroyCategory($id)
    {
        $category = Category::find($id);
        try{$category->delete();}
        catch (\Exception $e) {
            Session::flash('message', 'Not possible to delete the category, probably still has messages.');
            return Redirect::back();
          }
        Session::flash('message', 'Category deleted!');
        return Redirect::back();
    }
}
