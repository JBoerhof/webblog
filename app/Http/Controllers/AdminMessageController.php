<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use Session;
use Redirect;

class AdminMessageController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        $categories = \App\Category::All();
        $tags = \App\Tag::All();

        return view('admin/addmessage', compact('categories', 'tags'));
    }

    public function store(Request $request)
    {
        request()->validate([
            'user_id' => 'required',
            'category_id' => 'required',
            'title' => 'required',
            'discription' => 'required'
        ]);
        
        Message::create(request(['user_id', 'category_id', 'title', 'discription']));
        
        $message = Message::orderBy('created_at', 'desc')->first();
        
        $message->tag()->attach($request->input('tags'));

        Session::flash('message', "Message added!");
    
        return Redirect('/admin/category');
    }

    public function show(Message $message)
    {
        //
    }

    public function edit(Message $message)
    {
        $tags = \App\Tag::All();
     
        return view('admin/editmessage', compact('message', 'tags'));
    }

    public function update(Request $request, Message $message)
    {
        request()->validate([
            'title' => 'required',
            'discription' => 'required'
        ]);
              
        $message->title = request('title');
        $message->discription = request('discription');
        
        $message->tag()->sync($request->input('tags'));

        $message->save();
                
        Session::flash('message', 'Message edited!');
        
        return Redirect::back();
    }

    public function destroy(Message $message)
    {
        $message->delete();

        Session::flash('message', 'Message deleted!');
        
        return Redirect('/admin/category');
    }
}
