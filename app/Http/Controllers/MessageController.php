<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use Session;
use Redirect;

class MessageController extends Controller
{

    public function show(Message $message)
    {
        return view('content/message', compact('message'));
    }

}
