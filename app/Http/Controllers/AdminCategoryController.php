<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Session;
use Redirect;

class AdminCategoryController extends Controller
{

    public function index()
    {
        $categories = \App\Category::orderBy('created_at', 'DESC')->get();

        return view('/admin/admin', compact('categories'));
    }

    public function create()
    {
        return view('/admin/addcategory', compact('categories'));
    }

    public function store(Request $request)
    {
        request()->validate([
            'category' => 'required'
        ]);
        
        Category::create(['category' => request('category')]);

        Session::flash('message', "Category added!");

        return Redirect('/admin/category');
    }

    public function show(Category $category)
    {
        //
    }

    public function edit(Category $category)
    {
        return view('/admin/editcategory', compact('category'));
    }

    public function update(Request $request, Category $category)
    {
        request()->validate([
            'category' => 'required'
        ]);
                
        $category->category = request('category');

        $category->save();
                
        Session::flash('message', 'Category edited!');
        
        return Redirect('/admin/category');
    }

    public function destroy(Category $category)
    {
        try{$category->delete();}
        catch (\Exception $e) {
            Session::flash('message', 'Not possible to delete the category, probably still has messages.');
            return Redirect::back();
          }
        Session::flash('message', 'Category deleted!');
        return Redirect('/admin/category');
    }
}
