<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Session;
use Redirect;

class AdminCommentController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Comment $comment)
    {
        //
    }

    public function edit(Comment $comment)
    {
        //
    }

    public function update(Request $request, Comment $comment)
    {
        //
    }

    public function destroy(Comment $comment)
    {
        $messageid = $comment->message_id;

        $comment->delete();

        Session::flash('message', 'Comment deleted!');
        
        return Redirect('/admin/message/' . $messageid . '/edit');
    }
}
