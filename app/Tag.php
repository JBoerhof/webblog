<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function message(){
        return $this->belongsToMany('App\Tag', 'pivot_message_tag', 'message_id', 'tag_id')->withPivot('id');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tag'
    ];

}
