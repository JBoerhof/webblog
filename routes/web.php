<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'CategoryController@index');

Route::resource('category', 'CategoryController');
Route::resource('message', 'MessageController');
Route::resource('comment', 'CommentController');

Auth::routes();

Route::get('/admin', 'AdminCategoryController@index')->middleware('auth');
Route::get('/users', 'AdminUserController@index')->middleware('auth');

Route::resource('admin/category', 'AdminCategoryController')->middleware('auth');
Route::resource('admin/message', 'AdminMessageController')->middleware('auth');
Route::resource('admin/comment', 'AdminCommentController')->middleware('auth');
Route::resource('admin/tag', 'AdminTagController')->middleware('auth');

Route::resource('admin/user', 'AdminUserController')->middleware('auth');




// Route::get('/', 'PostsController@index');

// Route::get('/messages/{id}', 'PostsController@indexMessages');
// Route::post('/message', 'PostsController@store');
// Route::get('/message/{id}', 'PostsController@show');

// Auth::routes();
// //Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

// Route::get('/addmessage', 'PostsController@createMessage')->middleware('auth');
// Route::get('/addcategory', 'PostsController@createCategory')->middleware('auth');
// Route::get('/addtag', 'PostsController@createTag')->middleware('auth');

// Route::get('/editmessage/{id}', 'PostsController@editMessage')->middleware('auth');
// Route::get('/editcategory/{id}', 'PostsController@editCategory')->middleware('auth');
// Route::get('/edittag/{id}', 'PostsController@editTag')->middleware('auth');

// Route::post('/addmessage', 'PostsController@store')->middleware('auth');
// Route::post('/addcategory', 'PostsController@store')->middleware('auth');
// Route::post('/addtag', 'PostsController@store')->middleware('auth');

// Route::patch('/editcategory/{id}', 'PostsController@updateCategory')->middleware('auth');
// Route::patch('/editmessage/{id}', 'PostsController@updateMessage')->middleware('auth');
// Route::patch('/edittag/{id}', 'PostsController@updateTag')->middleware('auth');

// Route::get('/deletemessage/{id}', 'PostsController@destroyMessage')->middleware('auth');
// Route::delete('/deletecomment/{id}', 'PostsController@destroyComment')->middleware('auth');
// Route::get('/deletetag/{id}', 'PostsController@destroyTag')->middleware('auth');
// Route::get('/deletecategory/{id}', 'PostsController@destroyCategory')->middleware('auth');